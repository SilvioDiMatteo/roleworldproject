﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class BundleBuilder : Editor
{
    [MenuItem("Assets/ Build AssetBundles")]
    static void BuildAllAssetBundles()
    {
        if (!System.IO.Directory.Exists(FilesPaths.ASSET_BUNDLES_PATH))
        {
            System.IO.Directory.CreateDirectory(FilesPaths.ASSET_BUNDLES_PATH);
        }
        BuildPipeline.BuildAssetBundles(FilesPaths.ASSET_BUNDLES_PATH, BuildAssetBundleOptions.ChunkBasedCompression, BuildTarget.StandaloneWindows64);
    }

}
