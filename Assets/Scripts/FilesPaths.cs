﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FilesPaths
{
    public static string SESSIONS_PATH = Application.streamingAssetsPath + "/Sessions";
    public static string ASSET_BUNDLES_PATH = Application.streamingAssetsPath + "/AssetBundles";
}
