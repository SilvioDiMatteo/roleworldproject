﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public class FilesUtilities
{
    public static void SaveTerrainInfos(JSONNode infos)
    {   
        System.IO.File.WriteAllText(FilesPaths.SESSIONS_PATH + "/0/Levels/terrain.info", infos.ToString());
    }

    public static JSONNode LoadTerrainInfos()
    {
        if (!System.IO.File.Exists(FilesPaths.SESSIONS_PATH + "/0/Levels/terrain.info")) { return null; }
        JSONNode infos = JSONNode.Parse(System.IO.File.ReadAllText(FilesPaths.SESSIONS_PATH + "/0/Levels/terrainInfo.txt"));
        return infos;
    }

    /// <summary>
    /// Compress and saves a terrain Alphamap in a Binary File
    /// </summary>
    /// <param name="terrainData"></param>
    public static bool SaveTerrainAlphamap(TerrainData terrainData, out int layers)
    {
        layers = 0;
        try
        {
            float[,,] alphamap = terrainData.GetAlphamaps(0, 0, terrainData.alphamapWidth, terrainData.alphamapHeight);
            using (var file = System.IO.File.Open(FilesPaths.SESSIONS_PATH + "/0/Levels/alphamap.data", System.IO.FileMode.Create))
            using (var writer = new System.IO.BinaryWriter(file))
            {
                for (int k = 0; k < terrainData.alphamapLayers; k++)
                {
                    if (IgnoreUnusedLayer(k, terrainData)) { continue; }
                    layers++;
                    for (int i = 0; i < terrainData.alphamapWidth; i++)
                    {
                        for (int j = 0; j < terrainData.alphamapHeight; j++)
                        {
                            byte value = (byte)(alphamap[i, j, k] * 100);
                            writer.Write(value);
                        }
                    }
                }
                writer.Close();
                file.Close();
            }
            return true;
        }
        catch (System.Exception ex)
        {
            Debug.LogError(ex);
            return false;
        }
    }

    static bool IgnoreUnusedLayer(int layerIndex, TerrainData terrainData)
    {
        float[,,] data = terrainData.GetAlphamaps(0, 0, terrainData.alphamapWidth, terrainData.alphamapHeight);
        for (int i = 0; i < terrainData.alphamapWidth; i++)
        {
            for (int j = 0; j < terrainData.alphamapHeight; j++)
            {
                if (data[i, j, layerIndex] > 0)
                {
                    return false;
                }
            }
        }
        return true;
    }

    /// <summary>
    /// Loads and Decompress a terrain Alphamap from a Binary File
    /// </summary>
    /// <returns></returns>
    public static float[,,] LoadTerrainAlphamap(int width, int height, int layers)
    {
        using (var file = System.IO.File.Open(FilesPaths.SESSIONS_PATH + "/0/Levels/alphamap.data", System.IO.FileMode.Open))
        using (var reader = new System.IO.BinaryReader(file))
        {
            float[,,] alphamap = new float[width, height, layers];

            for (int k = 0; k < layers; k++)
            {
                for (int i = 0; i < width; i++)
                {
                    for (int j = 0; j < height; j++)
                    {
                        byte value = reader.ReadByte();
                        alphamap[i, j, k] = (float)value / 100f;
                    }
                }
            }
            reader.Close();
            file.Close();
            return alphamap;
        }
    }

    public static void SaveTerrainDetailLayers(TerrainData terrainData)
    {
        using (var file = System.IO.File.Open(FilesPaths.SESSIONS_PATH + "/0/Levels/detailLayers.data", System.IO.FileMode.Create))
        using (var writer = new System.IO.BinaryWriter(file))
        {
            for (int i = 0; i < terrainData.detailPrototypes.Length; i++)
            {
                int[,] detailLayer = terrainData.GetDetailLayer(0, 0, terrainData.detailWidth, terrainData.detailHeight, i);

                for (int x = 0; x < terrainData.detailWidth; x++)
                {
                    for (int y = 0; y < terrainData.detailHeight; y++)
                    {
                        writer.Write(detailLayer[x, y]);
                    }
                }
                writer.Close();
                file.Close();
            }
        }
    }

    public static int[,,] LoadTerrainDetailLayers(int detailWidth, int detailHeight, int detailPrototypes)
    {
        int[,,] detailLayers = new int[detailWidth, detailHeight, detailPrototypes];
        using (var file = System.IO.File.Open(FilesPaths.SESSIONS_PATH + "/0/Levels/detailLayers.data", System.IO.FileMode.Open))
        using (var reader = new System.IO.BinaryReader(file))
        {
            for (int i = 0; i < detailPrototypes; i++)
            {
                for (int x = 0; x < detailWidth; x++)
                {
                    for (int y = 0; y < detailHeight; y++)
                    {
                        detailLayers[x, y, i] = reader.ReadInt32();
                    }
                }
                reader.Close();
                file.Close();
            }
        }
        return detailLayers;
    }
}
