﻿/* ----------------------------------------------
 * 
 * 				#PROJECTNAME#
 * 
 * Original Author: Silvio Di Matteo
 * Creation Date: #CREATIONDATE#
 * Updates: 
 * 
 * Copyright � StarworkGC
 * ----------------------------------------------
 */
using UnityEngine;
using System.Collections;
using SmartMVC;
using SimpleJSON;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

/// <summary>
/// 
/// </summary>
public class LevelEditorController : Controller<GameApplication>
{
    #region References

    LevelEditorModel levelEditorModel { get { return app.model.levelEditorModel; } }
    LevelEditorView levelEditorView { get { return app.view.levelEditorView; } }

    #endregion

    void Start()
    {
        ListenToEvent(Events.SAVE_LEVEL, SaveLevel);
        ListenToEvent(Events.LOAD_LEVEL, LoadLevel);
        ListenToEvent(Events.CREATE_TERRAIN, CreateTerrain);

        levelEditorModel.terrainInformations = JSONNode.Parse("{}");
        LoadAvailableTextures();
    }

    void OnDestroy()
    {
        StopListenToEvent(Events.SAVE_LEVEL, SaveLevel);
        StopListenToEvent(Events.LOAD_LEVEL, LoadLevel);
        StopListenToEvent(Events.CREATE_TERRAIN, CreateTerrain);
    }

    void Update()
    {
        if (!levelEditorModel.terrain) { return; }
        if (!Input.GetMouseButton(0)) { return; }
        if (!levelEditorModel.placingGrass)
        {
            PaintTerrain();
            return;
        }
        //PlaceGrass();
    }

    void PaintTerrain()
    {
        if (levelEditorModel.terrainLayerIndex < 0) { return; }
        Ray ray = levelEditorModel.myCamera.ScreenPointToRay(Input.mousePosition);
        if (!Physics.Raycast(ray, out RaycastHit hit, 100)) { return; }

        int centerX = Convert.ToInt32(((hit.point.x - levelEditorModel.terrain.transform.position.x) / levelEditorModel.terrain.terrainData.size.x) * levelEditorModel.terrain.terrainData.alphamapWidth);
        int centerY = Convert.ToInt32(((hit.point.z - levelEditorModel.terrain.transform.position.z) / levelEditorModel.terrain.terrainData.size.x) * levelEditorModel.terrain.terrainData.alphamapHeight);

        for (int x = -levelEditorModel.brushRadius; x < levelEditorModel.brushRadius; x++)
        {
            int height = (int)Math.Sqrt(levelEditorModel.brushRadius * levelEditorModel.brushRadius - x * x);
            for (int y = -height; y < height; y++)
            {
                if (((y + centerY) >= 0) && ((x + centerX) >= 0) &&
                    ((y + centerY) < levelEditorModel.terrain.terrainData.alphamapHeight) && ((x + centerX) < levelEditorModel.terrain.terrainData.alphamapWidth))
                {
                    for (int i = 0; i < levelEditorModel.terrain.terrainData.alphamapLayers; i++)
                    {
                        int layerValue = (i == levelEditorModel.terrainLayerIndex) ? 1 : 0;
                        levelEditorModel.alphamapData[y + centerY, x + centerX, i] = levelEditorModel.element[0, 0, i] = layerValue;
                    }
                }
            }
        }
        levelEditorModel.terrain.terrainData.SetAlphamaps(0, 0, levelEditorModel.alphamapData);
    }

    /// <summary>
    /// Saves level data on disk
    /// </summary>
    /// <param name="data">Terrain's data</param>
    void SaveLevel(params object[] data)
    {
        FilesUtilities.SaveTerrainAlphamap(levelEditorModel.terrain.terrainData, out int alphamapLayers);
        levelEditorModel.terrainInformations.Add("alphamapWidth", levelEditorModel.terrain.terrainData.alphamapWidth.ToString());
        levelEditorModel.terrainInformations.Add("alphamapHeight", levelEditorModel.terrain.terrainData.alphamapHeight.ToString());
        levelEditorModel.terrainInformations.Add("alphamapLayers", alphamapLayers.ToString());
        FilesUtilities.SaveTerrainDetailLayers(levelEditorModel.terrain.terrainData);
        levelEditorModel.terrainInformations.Add("detailWidth", levelEditorModel.terrain.terrainData.detailWidth.ToString());
        levelEditorModel.terrainInformations.Add("detailHeight", levelEditorModel.terrain.terrainData.detailHeight.ToString());
        levelEditorModel.terrainInformations.Add("detailPrototypes", levelEditorModel.terrain.terrainData.detailPrototypes.Length.ToString());
        FilesUtilities.SaveTerrainInfos(levelEditorModel.terrainInformations);
    }

    /// <summary>
    /// Loads terrain informations and applies them to the current terrain
    /// </summary>
    /// <param name="data"></param>
    void LoadLevel(params object[] data)
    {
        levelEditorModel.terrainInformations = FilesUtilities.LoadTerrainInfos();
        levelEditorModel.terrain.terrainData.terrainLayers = LoadTerrainLayers();
        levelEditorModel.alphamapData = FilesUtilities.LoadTerrainAlphamap
        (
            levelEditorModel.terrainInformations["alphamapWidth"].AsInt,
            levelEditorModel.terrainInformations["alphamapHeight"].AsInt,
            levelEditorModel.terrainInformations["alphamapLayers"].AsInt
        );
        levelEditorModel.terrain.terrainData.SetAlphamaps(0, 0, levelEditorModel.alphamapData);
        levelEditorModel.element = new float[1, 1, levelEditorModel.terrain.terrainData.alphamapLayers];
    }

    /// <summary>
    /// Creates a new terrain
    /// </summary>
    /// <param name="data"></param>
    void CreateTerrain(params object[] data)
    {
        TerrainData terrainData = new TerrainData();
        terrainData.size = new Vector3(20, 20, 20);
        levelEditorModel.terrain = Terrain.CreateTerrainGameObject(terrainData).GetComponent<Terrain>();
        levelEditorModel.alphamapData = levelEditorModel.terrain.terrainData.GetAlphamaps(0, 0, levelEditorModel.terrain.terrainData.alphamapWidth, levelEditorModel.terrain.terrainData.alphamapHeight);
        levelEditorModel.element = new float[1, 1, levelEditorModel.terrain.terrainData.alphamapLayers];
        levelEditorModel.terrain.terrainData.SetDetailResolution(1024, 32);
    }

    /// <summary>
    /// Loads used terrain layers
    /// </summary>
    /// <returns></returns>
    TerrainLayer[] LoadTerrainLayers()
    {
        AssetBundle bundle = AssetBundle.LoadFromFile(FilesPaths.ASSET_BUNDLES_PATH + "/terraintextures");
        TerrainLayer[] layers = new TerrainLayer[2];
        for (int i = 0; i < layers.Length; i++)
        {
            layers[i] = new TerrainLayer();
        }
        layers[0].diffuseTexture = bundle.LoadAsset<Texture2D>("Grass (Meadows2).jpg");
        layers[1].diffuseTexture = bundle.LoadAsset<Texture2D>("Grass (Muddy).jpg");
        bundle.Unload(false);
        return layers;
    }

    /// <summary>
    /// Loads all the available textures in the inventory
    /// </summary>
    void LoadAvailableTextures()
    {
        AssetBundle bundle = AssetBundle.LoadFromFile(FilesPaths.ASSET_BUNDLES_PATH + "/terraintextures");
        Texture[] textures = bundle.LoadAllAssets<Texture>();
        int i = 0;

        foreach (Texture texture in textures)
        {
            Button button = Instantiate(levelEditorModel.inventoryItemPrefab).GetComponent<Button>();
            button.GetComponent<RawImage>().texture = texture;
            button.transform.SetParent(levelEditorView.inventoryScrollView.content, false);
            button.onClick.AddListener(() => OnButtonClick(texture));
            i++;
        }
        bundle.Unload(false);
    }
    
    /// <summary>
    /// Called when an inventory button is clicked
    /// </summary>
    /// <param name="texture">The inventory button's texture</param>
    void OnButtonClick(Texture texture)
    {
        AddTerrainLayer((Texture2D)(texture));
    }

    /// <summary>
    /// Adds a Terrain Layer to the terrain's data
    /// </summary>
    /// <param name="texture">The texture of the new Terrain Layer</param>
    void AddTerrainLayer(Texture2D texture)
    {
        if (!levelEditorModel.terrain) { return; }
        TerrainLayer[] layers = new TerrainLayer[levelEditorModel.terrain.terrainData.alphamapLayers + 1];
        Array.Copy(levelEditorModel.terrain.terrainData.terrainLayers, layers, levelEditorModel.terrain.terrainData.alphamapLayers);
        
        for (int i = 0; i < layers.Length; i++)
        {
            if (!layers[i]) { continue; }
            if (layers[i].diffuseTexture == texture)
            {
                levelEditorModel.terrainLayerIndex = i;
                return;
            }
        }   
        layers[layers.Length - 1] = new TerrainLayer { diffuseTexture = texture };
        levelEditorModel.terrain.terrainData.terrainLayers = layers;
        levelEditorModel.alphamapData = levelEditorModel.terrain.terrainData.GetAlphamaps(0, 0, levelEditorModel.terrain.terrainData.alphamapWidth, levelEditorModel.terrain.terrainData.alphamapHeight);
        levelEditorModel.element = new float[1, 1, levelEditorModel.terrain.terrainData.alphamapLayers];
        levelEditorModel.terrainLayerIndex = levelEditorModel.terrain.terrainData.alphamapLayers - 1;
    }
}