﻿/* ----------------------------------------------
 * 
 * 				#PROJECTNAME#
 * 
 * Original Author: Silvio Di Matteo
 * Creation Date: #CREATIONDATE#
 * Updates: 
 * 
 * Copyright © StarworkGC
 * ----------------------------------------------
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SmartMVC;

public class GameView : View<GameApplication>
{
    public LevelEditorView levelEditorView
    {
        get
        {
            myLevelEditorView = Assert<LevelEditorView>(myLevelEditorView);
            return myLevelEditorView;
        }
    }
    LevelEditorView myLevelEditorView;
}
