﻿/* ----------------------------------------------
 * 
 * 				#PROJECTNAME#
 * 
 * Original Author: Silvio Di Matteo
 * Creation Date: #CREATIONDATE#
 * Updates: 
 * 
 * Copyright � StarworkGC
 * ----------------------------------------------
 */
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SmartMVC;
using System.Collections.Generic;

/// <summary>
/// 
/// </summary>
public class LevelEditorView : View<GameApplication>
{
    public ScrollRect inventoryScrollView;

    public void OnClickSaveLevel()
    {
        Notify(Events.SAVE_LEVEL);
    }

    public void OnClickLoadLevel()
    {
        Notify(Events.LOAD_LEVEL);
    }

    public void OnClickCreateTerrain()
    {
        Notify(Events.CREATE_TERRAIN);
    }
}
