﻿/* ----------------------------------------------
 * 
 * 				#PROJECTNAME#
 * 
 * Original Author: Silvio Di Matteo
 * Creation Date: #CREATIONDATE#
 * Updates: 
 * 
 * Copyright © StarworkGC
 * ----------------------------------------------
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SmartMVC;

public class GameApplication : BaseApplication<GameModel, GameView, GameController>
{
    /// <summary>
    /// Reference to the singleton instance
    /// </summary>
    public static GameApplication singleton;

    protected override void Awake()
    {
        base.Awake();
        if ((singleton != null) && (singleton != this))
        {
            Destroy(gameObject);
            return;
        }
        singleton = this;
    }
}
