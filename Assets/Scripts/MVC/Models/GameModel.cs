﻿/* ----------------------------------------------
 * 
 * 				#PROJECTNAME#
 * 
 * Original Author: Silvio Di Matteo
 * Creation Date: #CREATIONDATE#
 * Updates: 
 * 
 * Copyright © StarworkGC
 * ----------------------------------------------
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SmartMVC;

public class GameModel : Model<GameApplication>
{
    public LevelEditorModel levelEditorModel
    {
        get
        {
            myLevelEditorModel = Assert<LevelEditorModel>(myLevelEditorModel);
            return myLevelEditorModel;
        }        
    }
    LevelEditorModel myLevelEditorModel;
}
