﻿/* ----------------------------------------------
 * 
 * 				#PROJECTNAME#
 * 
 * Original Author: Silvio Di Matteo
 * Creation Date: #CREATIONDATE#
 * Updates: 
 * 
 * Copyright � StarworkGC
 * ----------------------------------------------
 */
using UnityEngine;
using System.Collections;
using SimpleJSON;
using SmartMVC;

/// <summary>
/// Contains all the data of the Level Editor
/// </summary>
public class LevelEditorModel : Model<GameApplication>
{
    public Terrain terrain;
    public JSONNode terrainInformations;
    public GameObject inventoryItemPrefab;
    public Camera myCamera;
    public float[,,] alphamapData;
    public float[,,] element;
    public int[,] detailMapData;
    public int brushSize = 20;
    public int brushRadius { get { return brushSize / 2; } }
    public int terrainLayerIndex = -1;
    public bool placingGrass = false;
}
