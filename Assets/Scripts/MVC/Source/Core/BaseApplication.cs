﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SmartMVC
{
    /// <summary>
    /// Extension of the BaseApplication class to handle different types of Model View Controllers.
    /// </summary>
    /// <typeparam name="M"></typeparam>
    /// <typeparam name="V"></typeparam>
    /// <typeparam name="C"></typeparam>
    public class BaseApplication<M, V, C> : BaseApplication
        where M : Element
        where V : Element
        where C : Element
    {
        /// <summary>
        /// Model reference using the new type.
        /// </summary>
        new public M model { get { return (M)(object)base.model; } }

        /// <summary>
        /// View reference using the new type.
        /// </summary>
        new public V view { get { return (V)(object)base.view; } }

        /// <summary>
        /// Controller reference using the new type.
        /// </summary>
        new public C controller { get { return (C)(object)base.controller; } }
    }

    /// <summary>
    /// Root class for the scene's scripts.
    /// </summary>
    public class BaseApplication : Element
    {
        /// <summary>
        /// Arguments to be passed between scenes.
        /// </summary>
        static List<string> _args { get { return m_args == null ? (m_args = new List<string>()) : m_args; } }
        static List<string> m_args;

        /// <summary>
        /// Flag that indicates the first scene was loaded.
        /// </summary>
        static bool m_first_scene;

        /// <summary>
        /// Little static init.
        /// </summary>
        static BaseApplication() { m_first_scene = true; }

        /// <summary>
        /// Arguments passed between scenes.
        /// </summary>
        public List<string> args { get { return m_args; } }

        /// <summary>
        /// Fetches the root Model instance.
        /// </summary>
        public Model model { get { return m_model = Assert<Model>(m_model); } }
        private Model m_model;

        /// <summary>
        /// Fetches the root View instance.
        /// </summary>
        public View view { get { return m_view = Assert<View>(m_view); } }
        private View m_view;

        /// <summary>
        /// Fetches the root Controller instance.
        /// </summary>
        public Controller controller { get { return m_controller = Assert<Controller>(m_controller); } }
        private Controller m_controller;

        /// <summary>
        /// The handler of every MVC event
        /// </summary>
        /// <param name="data"></param>
        public delegate void OnNotificationSentHandler(params object[] data);

        /// <summary>
        /// Contains all the MVC events
        /// </summary>
        Dictionary<string, SmartMVCEvent> events = new Dictionary<string, SmartMVCEvent>();

        /// <summary>
        /// Notifies to the listening controllers the event
        /// </summary>
        /// <param name="eventName">The name of the event to notify</param>
        /// <param name="target">The object that called this method</param>
        /// <param name="data">The parameters to pass to the listening controllers</param>
        public void Notify(string eventName, Object target, params object[] data)
        {
            if (!events.ContainsKey(eventName))
            {
                Debug.LogError("No controller is listening to the event: " + eventName);
                return;
            }
            if (events[eventName] == null) { return; }
            events[eventName].CallEvent(data);
        }

        /// <summary>
        /// Attach a method to an event in the dictionary
        /// </summary>
        /// <param name="eventName">The name of the event to attach</param>
        /// <param name="method">The method to detach</param>
        /// <param name="target">The object that called this method</param>
        public void ListenToEvent(string eventName, OnNotificationSentHandler method, Object target)
        {
            if (events.ContainsKey(eventName))
            {
                events[eventName].SmartEvent += method;
                return;
            }
            SmartMVCEvent newSmartEvent = new SmartMVCEvent();
            newSmartEvent.SmartEvent += method;
            events.Add(eventName, newSmartEvent);
        }

        /// <summary>
        /// Detach a method from an event in the dictionary
        /// </summary>
        /// <param name="eventName">The name of the event to detach</param>
        /// <param name="method">The method to detach</param>
        /// <param name="target">The object that called this method</param>
        public void StopListenToEvent(string eventName, OnNotificationSentHandler method, Object target)
        {
            if (!events.ContainsKey(eventName)) { return; }
            if (events[eventName] != null)
            {
                events[eventName].SmartEvent -= method;
            }
            if (events[eventName].EventIsEmpty())
            {
                events.Remove(eventName);
            }
        }

        virtual protected void Awake()
        {
            if (m_first_scene) { m_first_scene = false;  }
        }
    }

    /// <summary>
    /// Represents a smart MVC event
    /// </summary>
    public class SmartMVCEvent
    {
        public event BaseApplication.OnNotificationSentHandler SmartEvent;
        public void CallEvent(params object[] data) { if (!EventIsEmpty()) { SmartEvent(data); } }
        public bool EventIsEmpty() { return SmartEvent == null; }
    }
}
