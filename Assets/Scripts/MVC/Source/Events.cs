﻿/* ----------------------------------------------
 * 
 * 				#PROJECTNAME#
 * 
 * Original Author: Silvio Di Matteo
 * Creation Date: #CREATIONDATE#
 * Updates: 
 * 
 * Copyright © StarworkGC
 * ----------------------------------------------
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SmartMVC
{
    /// <summary>
    /// Contains all the events of the SmartMVC system
    /// </summary>
    public class Events
    {
        public const string SAVE_LEVEL = "level.save";
        public const string LOAD_LEVEL = "level.load";
        public const string CREATE_TERRAIN = "terrain.create";

    }
}
