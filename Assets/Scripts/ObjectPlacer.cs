﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPlacer : MonoBehaviour
{
    [SerializeField]
    GameObject objectToPlace;
    GameObject objectPreview;
    public bool instantiatingObject = false;
       
    Camera myCamera;
    //TerrainPainter terrainPainter;

    void Start()
    {
        myCamera = GetComponent<Camera>();
        //terrainPainter = GetComponent<TerrainPainter>();
        //terrainPainter.objectPlacer = this;
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.P))
        {
            instantiatingObject = !instantiatingObject;
            ShowObjectPreview(instantiatingObject);
        }
        if (!instantiatingObject) { return; }
        Ray ray = myCamera.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out RaycastHit hit, 100))
        {
            Vector3 objectPosition = new Vector3
            (
                (Mathf.CeilToInt(hit.point.x - GameApplication.singleton.model.levelEditorModel.terrain.transform.position.x) - 0.5f), 
                (GameApplication.singleton.model.levelEditorModel.terrain.transform.position.y + (objectToPlace.transform.localScale.y / 2)),
                (Mathf.CeilToInt(hit.point.z - GameApplication.singleton.model.levelEditorModel.terrain.transform.position.z) - 0.5f)
            );
            MoveObjectPreviewAt(objectPosition);
            if (Input.GetMouseButtonUp(0))
            {
                PlaceObjectAt(objectPosition);
            }
        }        
    }

    void ShowObjectPreview(bool show)
    {
        if (!show)
        {
            Destroy(objectPreview);
            return;
        }
        objectPreview = Instantiate(objectToPlace, Vector3.zero, Quaternion.identity);
    }

    void PlaceObjectAt(Vector3 position)
    {
        Instantiate(objectToPlace, position, Quaternion.identity);
    }

    void MoveObjectPreviewAt(Vector3 position)
    {
        objectPreview.transform.position = position;
    }
}
