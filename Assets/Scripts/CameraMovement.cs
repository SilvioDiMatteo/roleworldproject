﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    #region Constants
    const string HORIZONTAL_AXIS = "Horizontal";
    const string VERTICAL_AXIS = "Vertical";
    const string MOUSE_Y_AXIS = "Mouse Y";
    const string MOUSE_X_AXIS = "Mouse X";
    #endregion

    public byte speed;
    public byte rotationSpeed;
    public GameObject canvas;    

    void Update()
    {
        MoveCamera();
        RotateCamera();       
    }

    void MoveCamera()
    {
        float horizontal = Input.GetAxis(HORIZONTAL_AXIS);
        float vertical = Input.GetAxis(VERTICAL_AXIS);
        Vector3 newPosition = (transform.forward * vertical) + (transform.right * horizontal);

        transform.Translate(newPosition * Time.deltaTime * speed, Space.World);
    }

    void RotateCamera()
    {
        if (!Input.GetMouseButton(1)) { return; }
        float horizontal = Input.GetAxis(MOUSE_X_AXIS);
        float vertical = Input.GetAxis(MOUSE_Y_AXIS);

        Vector3 newRotation = transform.eulerAngles;
        newRotation.x -= vertical * rotationSpeed * Time.deltaTime;
        newRotation.y += horizontal * rotationSpeed * Time.deltaTime;

        transform.eulerAngles = newRotation;
    }   
}
